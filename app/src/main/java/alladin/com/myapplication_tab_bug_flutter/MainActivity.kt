package com.myapplication_tab_bug_flutter

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import io.flutter.embedding.android.FlutterActivity

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        findViewById<View>(R.id.bStartWithTabs)
            .setOnClickListener {

                startActivity(
                    FlutterActivity
                        .withCachedEngine("my_engine_id")
                        .build(this)
                )
            }
        findViewById<View>(R.id.bStartWithOutTabs)
            .setOnClickListener {

            }
    }
}