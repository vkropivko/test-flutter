import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttermoduletab/pb_tab_bar.dart';
import 'package:fluttermoduletab/pb_tabs.dart';

class TabBugPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return buildPbTabs();
  }

  PbTabs buildPbTabs() {
    return PbTabs(
      pages: {
        TabInfo(title: "First tab"): Column(children: <Widget>[
          Container(
            height: 50,
            color: Colors.blue,
            child: Text("first"),
          )
        ]),
        TabInfo(title: "Second tab"): Column(children: <Widget>[Container(
          color: Colors.red,
          height: 50,
          child: Text("second"),
        )])
      },
      defaultIndex: 1,
    );
  }
}
