import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttermoduletab/pb_tab_bar.dart';

const _tabBarSizeOnIOS = 40.0;
const _tabBarSizeOnAndroid = 42.0;

class PbTabs extends StatelessWidget {
  int index = 0;
  int defaultIndex = 0;
  Map<TabInfo, Widget> pages;
  final bool isScrollable = false;
  bool isSmallMode = false;
  OnTabChangeFun onTabChangeFun;

  PbTabs({this.pages, this.defaultIndex = 0}) {
    tabs.addAll(pages.keys);
    views.addAll(pages.values);
  }

  final tabs = <TabInfo>[];
  final views = <Widget>[];

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: tabs.length,
      child: Scaffold(
        body: Column(
          children: [
            PbTabBar(tabs: tabs),
            Expanded(child: TabBarView(children: views))
          ],
        ),
      ),
      initialIndex: defaultIndex,
    );
  }
}

class CustomTabController extends StatefulWidget {
  int length;
  TabFun tabBuilder;
  int defaultIndex;
  OnTabChangeFun onTabChange;

  CustomTabController(
      {this.length, this.tabBuilder, this.onTabChange, this.defaultIndex});

  @override
  CustomTabControllerState createState() => new CustomTabControllerState();
}

class CustomTabControllerState extends State<CustomTabController>
    with SingleTickerProviderStateMixin {
  TabController controller;

  @override
  void initState() {
    controller = new TabController(
        vsync: this,
        length: widget.length,
        initialIndex: widget.defaultIndex ?? 0);
    controller.addListener(() => widget.onTabChange(controller.index));
    super.initState();
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return widget.tabBuilder(controller);
  }
}

typedef TabFun = Widget Function(TabController);
typedef OnTabChangeFun = void Function(int selectedTab);
