import 'dart:io';
import 'package:flutter/material.dart';

class TabInfo {
  String title;
  IconData icon;

  TabInfo({this.title, this.icon});
}

class PbTabBar extends StatelessWidget {
  bool isSmallMode = false;
  GlobalKey keyTabBar;
  bool isScrollable;
  var tabs = <TabInfo>[];
  int selectedTabIndex;
  TabController tabController;

  PbTabBar(
      {this.tabs,
      this.isScrollable = false,
      this.keyTabBar,
      this.isSmallMode = false,
      this.tabController});

  @override
  Widget build(BuildContext context) {
    var tabBar = TabBar(
        controller: tabController,
        key: keyTabBar,
        indicatorWeight: Platform.isIOS ? 0 : 3,
        indicator: Platform.isIOS
            ? BoxDecoration(
                color: Colors.green,
                borderRadius: BorderRadius.circular(8.0),
              )
            : null,
        indicatorColor: Colors.green,
        unselectedLabelColor: Colors.black38,
        labelColor: Platform.isIOS ? Colors.white : Colors.green,
        isScrollable: isScrollable,
        onTap: (index) {
          FocusScope.of(context).requestFocus(FocusNode());
        },
        tabs: List.generate(tabs.length, (i) {
          return Platform.isIOS || isSmallMode
              ? _toIOSTab(i, tabs[i])
              : _toTab(i, tabs[i]);
        }));
    return Platform.isIOS
        ? Padding(
            padding: EdgeInsets.only(left: 16.0, right: 16.0, bottom: 8.0),
            child: Container(
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(8.0)),
              height: 32,
              padding: EdgeInsets.all(1.0),
              child: tabBar,
            ))
        : tabBar;
  }

  Widget _toTab(i, TabInfo tab) {
    return Tab(
      icon: null,
      text: tab.title.toUpperCase(),
    );
  }

  Widget _toIOSTab(i, TabInfo tab) {
    return Tab(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Text(
            tab.title,
            textAlign: TextAlign.center,
          )
        ],
      ),
    );
  }
}
